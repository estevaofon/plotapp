from flask import Flask, render_template, request, url_for, redirect, flash
from stringPlotting import Calculus

app = Flask(__name__)
c = Calculus()
app.secret_key = 'why would I tell you my secret key?'
denied_list = ['import', 'sys', 'process',
               'subprocess', 'system', 'call', 'open']


@app.route('/')
def form():
    #return render_template('static_page.html')
    #return render_template('form_submit.html')
    return render_template('form_submit.html')


@app.route('/plot', methods=['POST'])
def plot():
    name = request.form['function']
    eq = str(name)
    email = request.form['youremail']
    interval = str(email)
    try:
        for item in denied_list:
            if item in eq:
                raise ValueError('Forbidden operation: '+item)
        values, labels, lower, highter = c.plot(eq, interval)
        return render_template('chart2.html', values=values,
                               labels=labels, lower=lower,
                               highter=highter, eq=eq)
    except ValueError as err:
        flash('Dont!')
        print(err.args)
        return redirect(url_for('form'))

if __name__ == '__main__':
    app.run(debug=True)
