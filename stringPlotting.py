#!/usr/bin/python
import numpy
from numpy import sin, cos, tan, sinh, cosh, tanh, arccos, arcsin, arctan, arcsinh, arccosh, arctanh, e, log, log10, sqrt
import math


def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    lstr = '.'.join([i, (d+'0'*n)[:n]])
    return float(lstr)


class Calculus:
    def plot(self, eq, xAxis):
        if ('=' in eq):
            temp, eq = eq.split('=')
        if('ln' in eq):
            eq = eq.replace('ln', 'log')
        elif('log' in eq):
            eq = eq.replace('log', 'log10')
        if('^' in eq):
            eq = eq.replace('^', '**')
        xi, xf = xAxis.split('to')
        xf = xf.strip()
        xi = xi.replace('x from', '').strip()
        x, y = 0, 0
        x = numpy.linspace(int(xi), int(xf), 20)
        y = eval(eq)
        t_x = []
        t_y = []
        x = list(x)
        y = list(y)
        for xi in x:
            t_x.append(truncate(xi, 2))
        for yi in y:
            t_y.append(truncate(yi, 2))
        labels = t_x
        values = t_y
        highter = int(math.ceil(max(t_y)))
        lower = int(math.floor(min(t_y)))
        if lower < 0:
            highter *= 2
        return (values, labels, lower, highter)
